package com.example.practicasqlite;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.practicasqlite.BaseDeDatos.OpenHelper;

public class LoginActivity extends AppCompatActivity {

    EditText txtUsuario, txtContrasenia;
    Button btnLogin;
    String usuario, contrasenia;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContrasenia = findViewById(R.id.txtContrasenia);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario = txtUsuario.getText().toString();
                contrasenia = txtContrasenia.getText().toString();
                IniciarSesion(usuario,contrasenia);
            }
        });

    }

    private void IniciarSesion(String usuario, String contrasenia) {
        if (!usuario.isEmpty() && !contrasenia.isEmpty()){
            try {
                OpenHelper conexion = new OpenHelper(this, "FACCI", null, 1);
                SQLiteDatabase db = conexion.getReadableDatabase();
                Cursor fila = db.rawQuery("select usuario, contrasenia from Usuario where id=1", null);
                if (fila.moveToFirst()){
                    String UserBD, ContraBD;
                    UserBD = fila.getString(0);
                    ContraBD = fila.getString(1);

                    if (usuario.equals(UserBD) && contrasenia.equals(ContraBD)){
                        Log.e("BDSQLITE", "LOGUEAR");
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(this, "DATOS INCORRECTOS", Toast.LENGTH_SHORT).show();
                    }
                    db.close();
                }
            }catch (Exception e){
                Log.e("BDSQLITE", e.getMessage());
            }

        }else if (usuario.isEmpty()){
            txtUsuario.setError("CAMPO OBLIGATORIO");
            txtUsuario.requestFocus();
        }else if (contrasenia.isEmpty()){
            txtContrasenia.setError("CAMPO OBLIGATORIO");
            txtContrasenia.requestFocus();
        }
    }


}
