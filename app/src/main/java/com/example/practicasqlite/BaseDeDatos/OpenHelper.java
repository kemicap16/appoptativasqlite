package com.example.practicasqlite.BaseDeDatos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelper extends SQLiteOpenHelper {

    public OpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Sentencia SQL que crea la tabla jugadores
        db.execSQL("create table Jugador(id int PRIMARY KEY UNIQUE, nombre text, edad int)");
        db.execSQL("CREATE TABLE Usuario(id int PRIMARY KEY, usuario text, contrasenia text)");
        db.execSQL("INSERT INTO Usuario VALUES (1,'KellyCastillo', '123456')");
    }

    //onUpgrade Metodo por si se vuelve a instalar nuestra app
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists Jugadores");
        db.execSQL("DROP TABLE IF EXISTS Usuario");
        onCreate(db);
    }
}
