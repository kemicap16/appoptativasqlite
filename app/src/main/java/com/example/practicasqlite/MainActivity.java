package com.example.practicasqlite;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.practicasqlite.BaseDeDatos.OpenHelper;

public class MainActivity extends AppCompatActivity {

    Button btnVer, btnBuscar, btnAgregar, btnEliminarJugadores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnVer = (Button) findViewById(R.id.btnVerJugadores);
        btnBuscar = (Button) findViewById(R.id.btnBuscarJugadores);
        btnAgregar = (Button) findViewById(R.id.btnAgregarJugadores);
        btnEliminarJugadores = findViewById(R.id.btnEliminarJugadores);

        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MostrarJugadoresActivity.class);
                startActivity(intent);
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BuscarJugadorActivity.class);
                startActivity(intent);
            }
        });

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AgregarJugadoresActivity.class);
                startActivity(intent);
            }
        });

        btnEliminarJugadores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    OpenHelper conexion = new OpenHelper(MainActivity.this, "FACCI", null, 1);
                    SQLiteDatabase db = conexion.getWritableDatabase();
                    db.delete("Jugador",null,null);
                    db.close();
                    Toast.makeText(getApplicationContext(), "SE HAN ELIMINADO TODOS LOS JUGADORES", Toast.LENGTH_SHORT);
                }catch (Exception e){

                }
            }
        });

    }
}
